/*
 * filter.h
 *
 * Copyright (C) 2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef FILTER_H
# define FILTER_H

# include <stdio.h>

typedef int (filter_func_t)(FILE * in, FILE * out)
    __attribute__((nonnull));
void print_error      (const char * fmt, ...)
    __attribute__((format(printf, 1, 2)));
void print_error_errno(const char * fmt, ...)
    __attribute__((format(printf, 1, 2)));
int filter            (const char * program_name,
                       filter_func_t * filter_func,
                       int argc,
                       char * argv[])
    __attribute__((nonnull));

#endif /* !FILTER_H */
