/*
 * ruc.c
 *
 * This file is part of lilrc-filters.
 *
 * A small program to remove unnecessary const declarations.
 *
 * Copyright (C) 2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "filter.h"

#define BUF_STEP 16

struct buf_s
{
    size_t siz;
    size_t len;
    char * buf;
};
typedef struct buf_s buf_t;

struct tworead_s
{
    buf_t      buffer;
    FILE *     file;

    /*
     * This is an array of positions where the different readers are in
     * the buffer. For this purpose, two readers are sufficient; one
     * being the main parser and the other being the parser checking if
     * a const is needed. 0 means the reader will read the next
     * character in the buffer or from the file. Any other value, n,
     * means that the reader will read the n:th byte from the buffer or
     * the first in the file next.
     *
     * The special value of SIZE_MAX is used to show that there is no
     * reader at the position. Here only the second reader will stop
     * this way.
     */
    size_t     poss[2]; /* positions */
};
typedef struct tworead_s tworead_t;

static void __attribute__((nonnull))
buf_init(buf_t * buf)
{
    buf->siz = 0;
    buf->len = 0;
    buf->buf = NULL;
    return;
}

static void __attribute__((nonnull))
buf_deinit(buf_t * buf)
{
    free(buf->buf);
    return;
}

static void __attribute__((nonnull))
buf_clear(buf_t * buf)
{
    buf->len = 0;
    return;
}

static int __attribute__((nonnull))
buf_alloc(buf_t * buf, size_t siz)
{
    char * new;

    if (buf->siz < siz)
    {
        new = realloc(buf->buf, siz);
        if (!new)
        {
            print_error_errno("could not allocate memory");
            return -1;
        }
        buf->buf = new;
        buf->siz = siz;
    }

    return 0;
}

static int
buf_append(buf_t * buf, char c)
{
    int ret;

    if (buf->len == buf->siz)
    {
        ret = buf_alloc(buf, buf->siz + BUF_STEP);
        if (ret)
        {
            return ret;
        }
    }

    buf->buf[buf->len] = c;
    buf->len++;

    return 0;
}

static void __attribute__((nonnull))
tworead_init(tworead_t * tr, FILE * file)
{
    buf_init(&tr->buffer);
    tr->file = file;
    tr->poss[0] = 0;
    tr->poss[1] = SIZE_MAX;
    return;
}

static void __attribute__((nonnull))
tworead_deinit(tworead_t * tr)
{
    buf_deinit(&tr->buffer);
    return;
}

static int __attribute__((nonnull))
tworead_read(tworead_t * tr, unsigned reader)
{
    size_t pos;
    size_t r;
    int    ret;
    char   c;

    assert(reader <= 1);
    assert(tr->poss[reader] != SIZE_MAX);

    pos = tr->poss[reader];
    if (pos < tr->buffer.len)
    {
        c = tr->buffer.buf[pos];
        tr->poss[reader]++;
        if (tr->poss[reader] == tr->buffer.len &&
            tr->poss[1] == SIZE_MAX)
        {
            tr->poss[reader] = 0;
            buf_clear(&tr->buffer);
        }
    }
    else
    {
        r = fread(&c, 1, 1, tr->file);
        if (!r)
        {
            if (ferror(tr->file))
            {
                print_error("error reading input");
                return -1;
            }
            else
            {
                return 0;
            }
        }

        if (tr->poss[1] != SIZE_MAX)
        {
            ret = buf_append(&tr->buffer, c);
            if (ret)
            {
                return -1;
            }
            tr->poss[reader]++;
        }
    }

    return c;
}

static void __attribute__((nonnull))
tworead_begin(tworead_t * tr)
{
    tr->poss[1] = tr->poss[0];
    return;
}

static void __attribute__((nonnull))
tworead_end(tworead_t * tr)
{
    tr->poss[1] = SIZE_MAX;
    return;
}

static int __attribute((nonnull(1)))
read_attr_char(tworead_t * tr, FILE * out, int reader)
{
    int c;

    c = tworead_read(tr, reader);
    if (c >= 0 && out)
    {
        c = fputc(c, out);
        if (c == EOF)
        {
            print_error("error writing output");
            return -1;
        }
    }

    return c;
}
static int __attribute__((nonnull(1)))
skip_attribute_content(tworead_t * tr, FILE * out, int reader, int c)
{
    unsigned depth = 1;

    while (isspace(c))
    {
        c = read_attr_char(tr, out, reader);
    }
    if (c < 0)
    {
        return -1;
    }

    if (c != '(')
    {
        fprintf(stderr, "ruc: warning: naked __attribute__\n");
        return 0;
    }
    if (out)
    {
        c = fputc(c, out);
        if (c == EOF)
        {
            print_error("error writing output");
            return -1;
        }
    }

    while (depth)
    {
        c = read_attr_char(tr, out, reader);
        if (c == '(')
        {
            depth++;
        }
        else if (c == ')')
        {
            depth--;
        }
        else if (c == 0)
        {
            fprintf(stderr, "ruc: warning: unfinished __attribute__\n");
            return 0;
        }
        else if (c < 0)
        {
            return -1;
        }
    }

    return 0;
}

static int __attribute__((nonnull))
skip_block(tworead_t * tr, int c)
{
    unsigned depth = 1;
    assert(c == '{');

    while (depth)
    {
        c = tworead_read(tr, 1);
        if (c <= 0)
        {
            /* eof or error */
            return c;
        }
        if (c == '{')
        {
            depth++;
        }
        else if (c == '}')
        {
            depth--;
        }
    }

    return tworead_read(tr, 1);
}

static int __attribute__((nonnull))
is_const_needed(tworead_t * tr, int c)
{
    /* holds the current word */
    buf_t         word;

    int           ret;
    int           needed = -1;

    tworead_begin(tr);
    buf_init(&word);

    /* Read words. */
    while (1)
    {
        /*
         * Read valid C word.
         */
        while (isalnum(c) || c == '_')
        {
            ret = buf_append(&word, c);
            if (ret)
            {
                goto end;
            }
            c = tworead_read(tr, 1);
        }
        if (c <= 0)
        {
            goto end;
        }

        /* Finish word. */
        ret = buf_append(&word, '\0');
        if (ret)
        {
            goto end;
        }

        if (word.len)
        {
            /*
             * Some words need special treatment.
             *
             * If the word is const then the (previous) const may well
             * be dropped.
             *
             * If the word is __attribute__ then the attribute content
             * must be skipped
             */
            if (strcmp(word.buf, "const") == 0)
            {
                needed = 0;
                goto end;
            }
            else if (strcmp(word.buf, "__attribute__") == 0)
            {
                ret = skip_attribute_content(tr, NULL, 1, c);
                if (ret)
                {
                    goto end;
                }
            }

            /* Word is not needed anymore. */
            buf_clear(&word);
        }

        /* Skip spaces. */
        while (isspace(c))
        {
            c = tworead_read(tr, 1);
        }
        if (c <= 0)
        {
            goto end;
        }

        if (c == '*' || c == '[')
        {
            /*
             * This is a pointer to const or an array of const, so the
             * const is needed.
             */
            needed = 1;
            goto end;
        }
        else if (c == '(' ||
                 c == ')' ||
                 c == ',' ||
                 c == ';' ||
                 c == '=')
        {
            /*
             * The declaration ended without being an array of const or
             * pointer to const so the const is not needed.
             */
            needed = 0;
            goto end;
        }
        else if (c == '{')
        {
            /*
             * Skip the block (it can be a enum, struct or union block.
             */
            c = skip_block(tr, c);
        }
        else if (!isalnum(c) && c != '_')
        {
            /* unexpected character, assume const is needed */
            fprintf(stderr,
                    "ruc: warning: unexpected character (%c)",
                    c);
            needed = 1;
            goto end;
        }
    }

end:
    /* Handle a stop of tworead_read. */
    if (c <= 0)
    {
        if (c < 0)
        {
            /* error */
        }
        else
        {
            /* premature eof, assume const needed */
            needed = 1;
        }
    }

    tworead_end(tr);
    buf_deinit(&word);
    return needed;
}

static int __attribute__((nonnull))
dump_buf(FILE * out, buf_t * buf)
{
    size_t w;

    w = fwrite(buf->buf, 1, buf->len, out);
    if (w < buf->len)
    {
        print_error("error writing output");
        return -1;
    }

    return 0;
}

static int __attribute__((nonnull))
handle_const(tworead_t * tr, FILE * out, buf_t * constbuf, int c)
{
    int ret;
    int answer;

    answer = is_const_needed(tr, c);
    if (answer < 0)
    {
        return -1;
    }

    /* If the const is needed, write it out. Otherwise dismiss it. */
    if (answer) {
        ret = dump_buf(out, constbuf);
        if (ret)
        {
            return -1;
        }
    }

    /*
     * Returning 1 here shows that this character, which has not yet
     * been parsed, should be reparsed.
     */
    return 1;
}

static int __attribute__((nonnull))
handle_attr(tworead_t * tr, FILE * out, buf_t * attrbuf, int c)
{
    /*
     * Returning 1 tells the parser that it should not print this
     * character (the leading parenthesis).
     */
    return dump_buf(out, attrbuf) ||
           skip_attribute_content(tr, out, 0, c) ||
           1;
}

/*
 * Returns -1 on error, 0 if the action was not fired or the return
 * value of the action if it was fired.
 */
static int __attribute__((nonnull))
check_for(const char * what,
          tworead_t * tr,
          FILE * out,
          buf_t * buf,
          int (*action)(tworead_t *, FILE *, buf_t *, int),
          int p,
          int c)
{
    size_t len = strlen(what);
    int    ret = 0;

    if (buf->len < len)
    {
        if (c == what[buf->len])
        {
            if (buf->len == 0)
            {
                if (!isalnum(p) && p != '_')
                {
                    ret = buf_append(buf, c);
                }
            }
            else
            {
                ret = buf_append(buf, c);
            }
        }
        else if (buf->len)
        {
            ret = dump_buf(out, buf);
            buf_clear(buf);
        }
    }
    else /* if (buf->len >= len) */
    {
        if (buf->len == len && (isalnum(c) || c == '_'))
        {
            /*
             * The word starts with the word to look for, but is longer
             * than that
             */
            buf_clear(buf);
        }
        else if (isspace(c))
        {
            ret = buf_append(buf, c);
        }
        else
        {
            ret = (*action)(tr, out, buf, c);
            buf_clear(buf);
        }
    }

    return ret;
}

static int __attribute__((nonnull))
ruc(FILE * in, FILE * out)
{
    buf_t     attrbuf;
    buf_t     constbuf;
    tworead_t tr;

    /* return values */
    size_t    w;
    int       retval         = 1;
    int       ret;

    /* parser state */
    int       in_multi       = 0;
    int       in_single      = 0;
    int       c              = '\0';
    int       p              = '\0';
    int       in_string      = '\0';

    buf_init(&attrbuf);
    buf_init(&constbuf);
    tworead_init(&tr, in);

    while ((c = tworead_read(&tr, 0)) > 0)
    {
        assert(in_string ? 1 : 0 + in_multi + in_single <= 1);

        if (in_string)
        {
            if (p != '\\')
            {
                if (c == in_string)
                {
                    in_string = '\0';
                }
            }
        }
        else if (in_multi)
        {
            if (p == '*' && c == '/')
            {
                in_multi = 0;
            }
        }
        else if (in_single)
        {
            if (p == '\n')
            {
                in_single = 0;
            }
        }
        else
        {
            if (c == '"' || c == '\'')
            {
                in_string = c;
            }
            else if (p == '/' && c == '*')
            {
                in_multi = 1;
            }
            else if (p == '/' && c == '/')
            {
                in_single = 1;
            }

            do {
                ret = check_for("const",
                                &tr,
                                out,
                                &constbuf,
                                handle_const,
                                p,
                                c);
            } while (ret > 0);
            if (ret == 0)
            {
                ret = check_for("__attribute__",
                                &tr,
                                out,
                                &attrbuf,
                                handle_attr,
                                p,
                                c);
            }
            if (ret < 0)
            {
                goto error;
            }
        }

        if (constbuf.len == 0 && attrbuf.len == 0 && !ret)
        {
            w = fwrite(&c, 1, 1, out);
            if (!w)
            {
                print_error("error writing output");
                goto error;
            }
        }

        ret = 0;
        p = c;
    }
    if (c < 0)
    {
        goto error;
    }

    retval = 0;
error:
    buf_deinit(&attrbuf);
    buf_deinit(&constbuf);
    tworead_deinit(&tr);
    return retval;
}

int
main(int argc, char * argv[])
{
    return filter("ruc", ruc, argc, argv);
}
