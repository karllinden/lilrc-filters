/*
 * The purpose of ruc is to remove unnecessary use of const, such as the
 * following declarations.
 */
static unsigned a = 7;
char b;

/*
 * The following is a caveat, which you should be aware of. ruc thinks
 * const is unnecessary here, although it might not be. Fixing this
 * would require some more logic.
 */
extern struct {
    int a;
    int b;
} some_struct;

/*
 * Another caveat is that ruc works poorly with function macros. It
 * confuses the macro with a function declaration or function
 * definition.
 */
#define UNSIGNED(a) unsigned a
UNSIGNED(char) ret[] = {'a', 'b', 'c', '\0'};

/* ruc works on function declarations ... */
unsigned bad_strlen_with_addition(const char * s, int);

/* ... as well as on function definitions. */
unsigned
bad_strlen_with_addition(const char * s, int i)
{
    unsigned l = 0;
    while (*s++) l++;
    return l + a + i;
}

/* It works with GCC attributes as well. */
static int __attribute__((const))
sum(int a, int b)
{
    return a + b;
}

/* If the code depends on implicit int, the result might look bad. */
function(void)
{
    return 1;
}

int
main(int argc, const char * argv[])
{
    const char * const * strict_argv = argv;

    /* ruc strips const in struct and unions properly. */
    struct {
        int a;
        const double * b;
    } struct_var;

    /* ruc does not strip const in front of arrays. */
    const long long array_of_const[] = {2, 3, 5, 7, 11};

    /*
     * Note that casts are scanned too, which means the following will
     * be removed, resulting in compilation failure. In general, it is
     * not nice to depend on implicit int.
     */
    int a = ()array_of_const[0];

    /*
     * ruc deletes excess const too, but note that it removes all but
     * the last.
     */
    char const * string = "hello";

    return 0;
}
