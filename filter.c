/*
 * c
 *
 * A simple simple library to create programs that work like .
 *
 * Copyright (C) 2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#define _XOPEN_SOURCE 500
#define _POSIX_C_SOURCE 200809L

#include <errno.h>
#include <ftw.h>
#include <limits.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <gop.h>

#include "filter.h"

static const char *    program_name = "unknown";
static char *          output       = NULL;
static int             in_place     = 0;
static char *          suffix       = NULL;
static int             recursive    = 0;
static filter_func_t * filter_func  = NULL;

void __attribute__((nonnull))
print_error_real(int err, const char * fmt, va_list ap)
{
    fprintf(stderr, "%s: error: ", program_name);
    vfprintf(stderr, fmt, ap);
    if (err)
    {
        fprintf(stderr, ": %s", strerror(err));
    }
    fputc('\n', stderr);
    return;
}

void __attribute__((format(printf, 1, 2)))
print_error(const char * fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    print_error_real(0, fmt, ap);
    va_end(ap);
    return;
}

void __attribute__((format(printf, 1, 2)))
print_error_errno(const char * fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    print_error_real(errno, fmt, ap);
    va_end(ap);
    return;
}

static int
process_file_in_place(const char * fpath,
                      const struct stat * sb,
                      int typeflags,
                      struct FTW * ftwbuf)
{
    const char * suffixes[] = {
        ".c",
        ".h",
        ".cc",
        NULL
    };

    const char ** strp;
    const char *  suf;
    size_t        len;
    size_t        suf_len;

    size_t        suffix_len;
    char *        backup  = NULL;

    int           retval  = 1;
    FILE *        in      = NULL;
    FILE *        out     = NULL;

    /*
     * memstream, in case no suffix is given (so input and output
     * collides)
     */
    char *        ms_buf  = NULL;
    size_t        ms_size = 0;

    int           ret;
    size_t        written;

    /*
     * Make sure the file matches one of the suffixes, or return
     * otherwise.
     */
    len = strlen(fpath);
    for (strp = suffixes; *strp != NULL; ++strp)
    {
        suf = *strp;
        suf_len = strlen(suf);
        if (suf_len <= len &&
            memcmp(fpath + len - suf_len, suf, suf_len) == 0)
        {
            break;
        }
    }
    if (!*strp)
    {
        /* The file did not have any of the suffixes. */
        return 0;
    }

    if (suffix)
    {
        suffix_len = strlen(suffix);
        backup = malloc(len + suffix_len + 1);
        if (!backup)
        {
            print_error_errno("could not allocate memory");
            goto error;
        }
        memcpy(backup, fpath, len);
        memcpy(backup + len, suffix, suffix_len + 1);

        ret = rename(fpath, backup);
        if (ret)
        {
            print_error_errno("could not rename %s to %s",
                              fpath, backup);
            goto error;
        }

        in = fopen(backup, "r");
        if (!in)
        {
            print_error_errno("could not open %s for reading", backup);
            goto error;
        }

        out = fopen(fpath, "w");
        if (!out)
        {
            print_error_errno("could not open %s for writing", fpath);
            goto error;
        }
    }
    else
    {
        in = fopen(fpath, "r");
        if (!in)
        {
            print_error_errno("could not open %s for writing", fpath);
            goto error;
        }

        out = open_memstream(&ms_buf, &ms_size);
        if (!out)
        {
            print_error_errno("open_memstream");
            goto error;
        }
    }

    ret = (*filter_func)(in, out);
    if (ret)
    {
        goto error;
    }

    ret = fclose(out);
    out = NULL;
    if (ret)
    {
        print_error_errno("could not close output");
        /* ignore error */
    }

    if (ms_buf)
    {
        out = fopen(fpath, "w");
        if (!out)
        {
            print_error_errno("could not open %s for writing", fpath);
            goto error;
        }

        written = fwrite(ms_buf, 1, ms_size, out);
        if (written < ms_size)
        {
            print_error("could not write everything to output");
            goto error;
        }
    }

    retval = 0;
error:
    if (in != NULL)
    {
        ret = fclose(in);
        if (ret)
        {
            print_error_errno("could not close input");
            /* ignore error */
        }
    }
    if (out != NULL)
    {
        ret = fclose(out);
        if (ret)
        {
            print_error_errno("could not close output");
            /* ignore error */
        }
    }
    free(backup);
    free(ms_buf);
    return retval;
}

static int
process_in_place(int argc, char * argv[])
{
    struct stat sb;
    int         ret = 0;

    for (int i = 1; i < argc; ++i)
    {
        ret = stat(argv[i], &sb);
        if (ret)
        {
            print_error_errno("could not stat %s", argv[i]);
            goto error;
        }

        if (S_ISDIR(sb.st_mode))
        {
            if (recursive)
            {
                ret = nftw(argv[i],
                           process_file_in_place,
                           16,
                           FTW_MOUNT | FTW_PHYS);
            }
            else
            {
                fprintf(stderr,
                        "%s: skipping %s\n", program_name, argv[i]);
            }
        }
        else
        {
            ret = process_file_in_place(argv[i], &sb, FTW_F, NULL);
        }
        if (ret)
        {
            goto error;
        }
    }
    return EXIT_SUCCESS;
error:
    return EXIT_FAILURE;
}

static int
process_to_output(int argc, char * argv[])
{
    int    exit_status = EXIT_FAILURE;

    char * real_input  = NULL;
    char * real_output = NULL;
    FILE * in          = NULL;
    FILE * out         = NULL;

    /* memstream, in case input and output collide */
    char *  ms_buf      = NULL;
    size_t  ms_size     = 0;

    int    ret;
    size_t written;

    if (output)
    {
        /*
         * If the output file collides with an input file, care must be
         * taken not to overwrite the output file before reading it.
         * Therefore, open an intermediate memory stream instead and
         * flush that into the input upon completion.
         */
        real_output = realpath(output, NULL);
        if (real_output)
        {
            for (int i = 1; i < argc; ++i)
            {
                real_input = realpath(argv[i], NULL);
                if (!real_input)
                {
                    print_error_errno("realpath");
                    goto error;
                }
                if (strcmp(real_input, real_output) == 0)
                {
                    out = open_memstream(&ms_buf, &ms_size);
                    if (!out)
                    {
                        print_error_errno("open_memstream");
                        goto error;
                    }
                }
                free(real_input);
            }
            free(real_output);
            real_input = NULL;
            real_output = NULL;
        }
        else if (errno != ENOENT)
        {
            /*
             * Only print an error if the file does not exist since a
             * non-existing output file will not collide with any input
             * file.
             */
            print_error_errno("realpath");
            goto error;
        }

        /*
         * If no intermediate output was opened, simply open the output
         * directly.
         */
        if (!out)
        {
            out = fopen(output, "w");
            if (!out)
            {
                print_error_errno("could not open output file for "
                                  "writing");
                goto error;
            }
        }
    }
    else
    {
        out = stdout;
    }

    if (argc <= 1)
    {
        ret = (*filter_func)(stdin, out);
        if (ret)
        {
            goto error;
        }
    }
    else
    {
        for (int i = 1; i < argc; ++i)
        {
            in = fopen(argv[i], "r");
            if (!in)
            {
                print_error_errno("could not open %s for reading",
                                  argv[i]);
                goto error;
            }
            ret = (*filter_func)(in, out);
            if (ret)
            {
                goto error;
            }
            ret = fclose(in);
            if (ret)
            {
                print_error_errno("could not close input");
                /* ignore error */
            }
        }
    }

    if (out != stdout)
    {
        ret = fclose(out);
        out = NULL;
        if (ret)
        {
            print_error_errno("could not close output");
            /* ignore error */
        }
    }

    if (ms_buf)
    {
        out = fopen(output, "w");
        if (!out)
        {
            print_error_errno("could not open %s for writing",
                              output);
            goto error;
        }

        written = fwrite(ms_buf, 1, ms_size, out);
        if (written < ms_size)
        {
            print_error("could not write everything to output");
            goto error;
        }
    }

error:
    if (out && out != stdout)
    {
        ret = fclose(out);
        if (ret)
        {
            print_error_errno("could not close output file");
        }
    }
    free(ms_buf);
    free(real_input);
    free(real_output);
    return exit_status;
}

static gop_return_t
args_callback(gop_t * gop)
{
    return GOP_DO_RETURN;
}

int
filter(const char * pn,
       filter_func_t * func,
       int argc,
       char * argv[])
{
    gop_t *  gop = NULL;

    program_name = pn;
    output       = NULL;
    in_place     = 0;
    suffix       = NULL;
    recursive    = 0;
    filter_func  = func;

    const gop_option_t options[] = {
        {"output", 'o', GOP_STRING, &output, NULL,
            "output file", "FILENAME"},
        {"in-place", 'i', GOP_NONE, &in_place, NULL,
            "reformat files in place", NULL},
        {"suffix", 's', GOP_STRING, &suffix, NULL,
            "make backups if files are reformatted in place", "SUFFIX"},
        {"recursive", 'R', GOP_NONE, &recursive, NULL,
            "reformat files recursively", NULL},
        {"args", '-', GOP_NONE, NULL, &args_callback,
            "treat remaining arguments as input files", NULL},
        GOP_TABLEEND
    };

    gop = gop_new();
    if (!gop)
    {
        return EXIT_FAILURE;
    }
    gop_set_program_name(gop, program_name);
    gop_add_table(gop, "Program options:", options);
    gop_add_usage(gop, "[OPTIONS...] [--|--args] [INPUT_FILES...]");
    gop_description(gop, "Reformat multiline C comments.");
    gop_extra_help(gop,
                   "The --output option cannot be used in conjunction "
                   "with the --in-place option. The --recursive option "
                   "implies --in-place.\n"
                   "\n"
                   "If no input file is given input is read from "
                   "standard in. If neither --output nor --in-place is "
                   "given, output is written to standard out.");
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);
    gop = NULL;

    if (recursive)
    {
        in_place = 1;
    }
    if (suffix && !in_place)
    {
        print_error("--suffix is given without --in-place");
        return EXIT_FAILURE;
    }
    if (output && in_place)
    {
        print_error("--output and --in-place cannot be used together");
        return EXIT_FAILURE;
    }
    if (in_place && argc < 2)
    {
        print_error("--in-place is given, but no file is given");
        return EXIT_FAILURE;
    }

    if (in_place)
    {
        return process_in_place(argc, argv);
    }
    else
    {
        return process_to_output(argc, argv);
    }
}
