#!/usr/bin/env python3
#
#  shell-replace.py
#
#  This file is part of lilrc-filters.
#
#  This program inserts the output of shell commands into a file.
#
#  Copyright (C) 2017 Karl Linden <karl.j.linden@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

import argparse
import subprocess
import sys

parser = argparse.ArgumentParser()
parser.add_argument(
        'infile',
        nargs='?',
        type=str,
        help='Insert shell output in a file',
        action='store',
        default=None)
parser.add_argument(
        '-o',
        '--output',
        help='Output file',
        type=str,
        action='store',
        dest='outfile',
        default=None)
args = parser.parse_args()

if args.infile:
    infile = open(args.infile, 'r')
else:
    infile = sys.stdin

if args.outfile:
    outfile = open(args.outfile, 'w')
else:
    outfile = sys.stdout

intext = infile.read()
while True:
    try:
        start = intext.index('{{')
        end = intext.index('}}', start)
    except ValueError:
        break
    outfile.write(intext[:start])
    commands = intext[start+2:end]
    intext = intext[end+2:]
    for row in commands.split('\n'):
        command = row.strip()
        if command:
            p = subprocess.Popen(
                    command,
                    shell=True,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.STDOUT)
            p.wait()
            out, err = p.communicate()
            outfile.write(out.decode('utf-8'))

outfile.write(intext)

infile.close()
outfile.close()
