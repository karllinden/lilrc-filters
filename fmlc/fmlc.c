/*
 * fmlc.c
 *
 * This file is part of lilrc-filters.
 *
 * A small program to format multiline comments.
 *
 * Copyright (C) 2017 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "filter.h"

static int
extend_buf(char ** bufp, size_t * bufsizp)
{
    char * new;
    size_t newsiz = *bufsizp + 16;

    new = realloc(*bufp, newsiz);
    if (!new)
    {
        print_error_errno("could not allocate memory");
        return 1;
    }
    *bufp = new;
    *bufsizp = newsiz;

    return 0;
}

static int
append_buf(char ** bufp, size_t * bufsizp, size_t * buflenp, char c)
{
    int ret;

    if (*buflenp == *bufsizp)
    {
        ret = extend_buf(bufp, bufsizp);
        if (ret)
        {
            return ret;
        }
    }

    (*bufp)[*buflenp] = c;
    (*buflenp)++;

    return 0;
}

static int
write_indent(unsigned indent, FILE * out)
{
    int ret;
    while (indent--)
    {
        ret = fputc(' ', out);
        if (ret == EOF)
        {
            print_error("error writing output");
            return 1;
        }
    }
    return 0;
}

static int __attribute__((nonnull))
fmlc(FILE * in, FILE * out)
{
    /* buffer variables */
    char *   buf            = NULL;
    size_t   bufsiz         = 0;
    size_t   buflen         = 0;

    /* loop variables */
    char *   next;
    char *   this;

    /* return values */
    size_t   r;
    size_t   w;
    int      retval         = 1;
    int      ret;

    /* parser state */
    int      in_multi       = 0;
    int      in_multi_block = 0;
    int      in_single      = 0;
    unsigned col            = 0;
    unsigned indent         = 0;
    unsigned indent_block   = 0;
    char     c              = '\0';
    char     p              = '\0';
    char     in_string      = '\0';

    while ((r = fread(&c, 1, 1, in)))
    {
        assert(in_string ? 1 : 0 + in_multi + in_single <= 1);

        if (c != '\n')
        {
            col++;
        }
        else
        {
            col = 0;
            indent = 0;
            in_single = 0;
        }

        if (isspace(c) && indent + 1 == col)
        {
            indent++;
        }

        if (in_string)
        {
            if (p != '\\')
            {
                if (c == in_string)
                {
                    in_string = '\0';
                }
            }
        }
        else if (in_multi_block)
        {
            /*
             * Do not include indentation in comment content buffer and
             * do not include the first asterisk (*) of a line in the
             * comment content, but be sure to include the line breaks
             * in order to not mess up some custom formatting of the
             * comment.
             */
            if (c == '\n' ||
                (indent != col && (col != indent + 1 || c != '*')))
            {
                ret = append_buf(&buf, &bufsiz, &buflen, c);
                if (ret)
                {
                    goto error;
                }
            }

            if (p == '*' && c == '/')
            {
                in_multi_block = 0;

                /*
                 * Remove comment ending and one space from the comment
                 * content buffer, since it will be unconditionally
                 * inserted later.
                 *
                 * Note that the asterisk will not be part of the
                 * comment content if the comment ending sequence was
                 * the first encountered on the line. Therefore check
                 * for the asterisk before removing it.
                 */
                buflen--;
                if (buf[buflen-1] == '*')
                {
                    buflen--;
                }
                if (buflen > 0 && buf[buflen-1] == ' ')
                {
                    buflen--;
                }

                /* Print the finalized comment. */
                if (memchr(buf, '\n', buflen))
                {
                    /*
                     * This is a proper multi line block comment. The
                     * last that was printed was the comment beginning
                     * (/ + *) which was printeded at indent_block.
                     *
                     * The comment ends unconditionally in a new line,
                     * so avoid duplicating an already existing ending
                     * newline.
                     */
                    if (buflen > 0 && buf[buflen-1] == '\n')
                    {
                        buflen--;
                    }

                    /*
                     * Since a the comment shall end in a new line,
                     * remove empty spaces so that no trailing spaces
                     * are introduced.
                     */
                    while (buflen > 0 && buf[buflen-1] == ' ')
                    {
                        buflen--;
                    }

                    /*
                     * Furthermore, the buffer is not NUL-terminated,
                     * but make it to ease the situation.
                     */
                    buf[buflen] = '\0';

                    /*
                     * If the block already started with a line
                     * containing only the comment beginning then make
                     * sure that the line break is not duplicated.
                     */
                    next = buf;
                    if (*next == '\n')
                    {
                        next++;
                    }
                    ret = fputc('\n', out);
                    if (ret == EOF)
                    {
                        print_error("error writing output");
                        goto error;
                    }

                    while (next)
                    {
                        this = next;
                        next = strchr(next, '\n');
                        if (next)
                        {
                            *next = '\0';
                            next++;
                        }

                        ret = write_indent(indent_block, out);
                        if (ret)
                        {
                            goto error;
                        }
                        ret = fputs(" *", out);
                        if (ret == EOF)
                        {
                            print_error("error writing output");
                            goto error;
                        }
                        if (*this)
                        {
                            ret = fputc(' ', out);
                            if (ret == EOF)
                            {
                                print_error("error writing output");
                                goto error;
                            }
                            if (*this == ' ')
                            {
                                this++;
                            }
                            ret = fputs(this, out);
                            if (ret == EOF)
                            {
                                print_error("error writing output");
                                goto error;
                            }
                        }
                        ret = fputc('\n', out);
                        if (ret == EOF)
                        {
                            print_error("error writing output");
                            goto error;
                        }
                    }

                    ret = write_indent(indent_block, out);
                    if (ret)
                    {
                        print_error("error writing output");
                        goto error;
                    }
                    ret = fputs(" *", out);
                    if (ret == EOF)
                    {
                        print_error("error writing output");
                        goto error;
                    }
                }
                else
                {
                    /*
                     * This is a one line comment enclosed in the C89
                     * standard comment delimiters. Just dump it. There
                     * shall be spacing between the comment beginning
                     * and the comment content. Be sure to restore
                     * spacing that was removed between the content and
                     * the comment ending.
                     */
                    if (buf[0] != ' ')
                    {
                        ret = fputc(' ', out);
                        if (ret == EOF)
                        {
                            print_error("error writing output");
                            goto error;
                        }
                    }
                    w = fwrite(buf, 1, buflen, out);
                    if (w < buflen)
                    {
                        print_error("error writing output");
                        goto error;
                    }
                    ret = fputs(" *", out);
                    if (ret == EOF)
                    {
                        print_error("error writing output");
                        goto error;
                    }
                }
                buflen = 0;
            }
        }
        else if (in_multi)
        {
            if (p == '*' && c == '/')
            {
                in_multi = 0;
            }
        }
        else if (in_single)
        {
            /* empty */
        }
        else
        {
            if (c == '"' || c == '\'')
            {
                in_string = c;
            }
            else if (p == '/' && c == '*')
            {
                if (indent + 2 == col)
                {
                    in_multi_block = 1;
                    indent_block = indent;
                    w = fwrite(&c, 1, 1, out);
                    if (!w)
                    {
                        print_error("error writing output");
                    }
                }
                else
                {
                    in_multi = 1;
                }
            }
            else if (p == '/' && c == '/')
            {
                in_single = 1;
            }
        }

        if (!in_multi_block)
        {
            w = fwrite(&c, 1, 1, out);
            if (!w)
            {
                print_error("error writing output");
                goto error;
            }
        }

        p = c;
    }
    if (ferror(in))
    {
        print_error("error reading input");
        goto error;
    }

    retval = 0;
error:
    free(buf);
    return retval;
}

int
main(int argc, char * argv[])
{
    return filter("fmlc", fmlc, argc, argv);
}
