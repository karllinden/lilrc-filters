/* The purpose of fmlc is to format multiline comments consistently,
 * that is this comment will be turned into a comment on the form which
 * is seen below. */

/*
 * This is how a properly formatted multiline comment looks. fmlc will
 * not change comments that look like this.
 */

/*
 * Empty rows are preserved withing properly formatted multiline
 * comments, as in this one, ...
 *
 * which consists, ...
 *
 *
 * of extra spacing.
 *
 * This kind of spacing is common in license headers, which not too
 * seldom end in a an extra empty row, like this comment.
 *
 */

/* Consistency is nice. Especially since there are a few ugly corner
 * cases, with this inconsistent commenting. If a fix textwidth is used
 * and the comment content does not overflow, but with the ending
 * sequence the comment does, the comment ending must be placed on the
 * next line which does not look very good, due to asymmetry. See here.
 */

/*
 * A comment might also have initial good space, but then ends
 * asymmetrically, like this comment, which fmlc fixes. */

/* One line comments are ignored. */
/*However, one line comments with insufficient initial spacing, ... */
/* or insufficient spacing on the right, ...*/
/*are spaced appropriately.*/

// Furthermore, one line comments on C++ form are also ignored.
