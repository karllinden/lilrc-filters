#
#  Makefile
#
#  This file is part of lilrc-filters.
#
#  Copyright (C) 2017 Karl Linden <karl.j.linden@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

include Makefile.common

subdirs := \
	fmlc \
	ruc

allsubdirs := $(subdirs)
all: all-subdirs
all-subdirs: $(allsubdirs)
$(allsubdirs): all-local
	UTIL=$@ $(MAKE) -I.. -f ../Makefile.subdir -C $@
.PHONY: all all-local all-subdirs $(allsubdirs)

checksubdirs := $(addprefix check-,$(subdirs))
check: check-local check-subdirs
check-subdirs: $(checksubdirs)
$(checksubdirs): all-local
	UTIL=$(@:check-%=%) $(MAKE) -I.. -f ../Makefile.subdir -C $(@:check-%=%) check
.PHONY: check check-local check-subdirs $(checksubdirs)

cleansubdirs := $(addprefix clean-,$(subdirs))
clean: clean-local clean-subdirs
clean-subdirs: $(cleansubdirs)
$(cleansubdirs): clean-%:
	UTIL=$(@:clean-%=%) $(MAKE) -I.. -f ../Makefile.subdir -C $(@:clean-%=%) clean
.PHONY: clean clean-local clean-subdirs $(cleansubdirs)

gitignoresubdirs := $(addprefix gitignore-,$(subdirs))
gitignore: gitignore-subdirs
gitignore-subdirs: $(gitignoresubdirs)
$(gitignoresubdirs): gitignore-%:
	UTIL=$(@:gitignore-%=%) $(MAKE) -I.. -f ../Makefile.subdir -C $(@:gitignore-%=%) .gitignore
.PHONY: gitignore gitignore-subdirs $(gitignoresubdirs)

installsubdirs := $(addprefix install-,$(subdirs))
install: install-subdirs
install-subdirs: $(installsubdirs)
$(installsubdirs): install-%: all-local
	UTIL=$(@:install-%=%) $(MAKE) -I.. -f ../Makefile.subdir -C $(@:install-%=%) install
.PHONY: install install-subdirs $(installsubdirs)

uninstallsubdirs := $(addprefix uninstall-,$(subdirs))
uninstall: uninstall-subdirs
uninstall-subdirs: $(uninstallsubdirs)
$(uninstallsubdirs): uninstall-%: all-local
	UTIL=$(@:uninstall-%=%) $(MAKE) -I.. -f ../Makefile.subdir -C $(@:uninstall-%=%) uninstall
.PHONY: uninstall uninstall-subdirs $(uninstallsubdirs)

all-local: filter.o
check-local:
clean-local:
	-rm -f filter.o

README.md: README.md.in
	./shell-replace.py $< -o $@
