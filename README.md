# lilrc-filters

Author: Karl Linden (lilrc)

Report bugs to: https://gitlab.com/lilrc/lilrc-filters/issues

Homepage: https://gitlab.com/lilrc/lilrc-filters

This package contains filters for C sources, that are used to format and
automate changing C sources.

## Install instructions

This package depends on [gop](https://gitlab.com/lilrc/gop) for parsing
command line options, so make sure that library is installed before
attempting to build this package.

To build the package, run:
```shell
make
```
and to install, run:
```shell
make install
```

## Format multi line comments

The `fmlc` filter formats multi line comments consistently. The below
diff shows the result of `fmlc`.
```diff
-/* The purpose of fmlc is to format multiline comments consistently,
+/*
+ * The purpose of fmlc is to format multiline comments consistently,
  * that is this comment will be turned into a comment on the form which
- * is seen below. */
+ * is seen below.
+ */
 
 /*
  * This is how a properly formatted multiline comment looks. fmlc will
  * not change comments that look like this.
  */
 
 /*
  * Empty rows are preserved withing properly formatted multiline
  * comments, as in this one, ...
  *
  * which consists, ...
  *
  *
  * of extra spacing.
  *
  * This kind of spacing is common in license headers, which not too
  * seldom end in a an extra empty row, like this comment.
  *
  */
 
-/* Consistency is nice. Especially since there are a few ugly corner
+/*
+ * Consistency is nice. Especially since there are a few ugly corner
  * cases, with this inconsistent commenting. If a fix textwidth is used
  * and the comment content does not overflow, but with the ending
  * sequence the comment does, the comment ending must be placed on the
  * next line which does not look very good, due to asymmetry. See here.
  */
 
 /*
  * A comment might also have initial good space, but then ends
- * asymmetrically, like this comment, which fmlc fixes. */
+ * asymmetrically, like this comment, which fmlc fixes.
+ */
 
 /* One line comments are ignored. */
-/*However, one line comments with insufficient initial spacing, ... */
-/* or insufficient spacing on the right, ...*/
-/*are spaced appropriately.*/
+/* However, one line comments with insufficient initial spacing, ... */
+/* or insufficient spacing on the right, ... */
+/* are spaced appropriately. */
 
 // Furthermore, one line comments on C++ form are also ignored.

```

## Remove unnecessary const

The `ruc` filter removes unnecessary use of `const`. A `const` keyword
is deemed necessary if it is part of "array of const"
(`const int array[]`) or "pointer to const" (`const char *`). Other
uses, such as "const pointer" (`* const`) are unnecessary.  However,
there are some (corner case) caveats, as the below example diff shows.
```diff
 /*
  * The purpose of ruc is to remove unnecessary use of const, such as the
  * following declarations.
  */
-static const unsigned a = 7;
-const char b;
+static unsigned a = 7;
+char b;
 
 /*
  * The following is a caveat, which you should be aware of. ruc thinks
  * const is unnecessary here, although it might not be. Fixing this
  * would require some more logic.
  */
-extern const struct {
+extern struct {
     int a;
     int b;
 } some_struct;
 
 /*
  * Another caveat is that ruc works poorly with function macros. It
  * confuses the macro with a function declaration or function
  * definition.
  */
 #define UNSIGNED(a) unsigned a
-const UNSIGNED(char) ret[] = {'a', 'b', 'c', '\0'};
+UNSIGNED(char) ret[] = {'a', 'b', 'c', '\0'};
 
 /* ruc works on function declarations ... */
-const unsigned bad_strlen_with_addition(const char * s, const int);
+unsigned bad_strlen_with_addition(const char * s, int);
 
 /* ... as well as on function definitions. */
-const unsigned
-bad_strlen_with_addition(const char * s, int const i)
+unsigned
+bad_strlen_with_addition(const char * s, int i)
 {
     unsigned l = 0;
     while (*s++) l++;
     return l + a + i;
 }
 
 /* It works with GCC attributes as well. */
 static int __attribute__((const))
-sum(const int a, const int b)
+sum(int a, int b)
 {
     return a + b;
 }
 
 /* If the code depends on implicit int, the result might look bad. */
-const
 function(void)
 {
     return 1;
 }
 
-const int
-main(const int argc, const char * argv[])
+int
+main(int argc, const char * argv[])
 {
-    const char * const * const strict_argv = argv;
+    const char * const * strict_argv = argv;
 
     /* ruc strips const in struct and unions properly. */
     struct {
-        const int a;
-        const double * const b;
+        int a;
+        const double * b;
     } struct_var;
 
     /* ruc does not strip const in front of arrays. */
     const long long array_of_const[] = {2, 3, 5, 7, 11};
 
     /*
      * Note that casts are scanned too, which means the following will
      * be removed, resulting in compilation failure. In general, it is
      * not nice to depend on implicit int.
      */
-    int a = (const)array_of_const[0];
+    int a = ()array_of_const[0];
 
     /*
      * ruc deletes excess const too, but note that it removes all but
      * the last.
      */
-    const const char const * string = "hello";
+    char const * string = "hello";
 
     return 0;
 }

```
